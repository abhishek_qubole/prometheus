/*! grafana - v4.5.2 - 2017-09-22
 * Copyright (c) 2017 Torkel Ödegaard; Licensed Apache-2.0 */

System.register(["lodash"],function(a,b){"use strict";var c,d;b&&b.id;return{setters:[function(a){c=a}],execute:function(){d=function(){function a(a,b){this.$scope=a,this.$injector=b,this.panel=this.panelCtrl.panel,this.isLastQuery=c.default.indexOf(this.panel.targets,this.target)===this.panel.targets.length-1}return a.prototype.refresh=function(){this.panelCtrl.refresh()},a}(),a("QueryCtrl",d)}}});