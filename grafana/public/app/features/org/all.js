/*! grafana - v4.5.2 - 2017-09-22
 * Copyright (c) 2017 Torkel Ödegaard; Licensed Apache-2.0 */

define(["./org_users_ctrl","./profile_ctrl","./org_users_ctrl","./select_org_ctrl","./change_password_ctrl","./newOrgCtrl","./userInviteCtrl","./orgApiKeysCtrl","./orgDetailsCtrl","./prefs_control"],function(){});