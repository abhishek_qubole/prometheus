/*! grafana - v4.5.2 - 2017-09-22
 * Copyright (c) 2017 Torkel Ödegaard; Licensed Apache-2.0 */

define(["./panellinks/module","./dashlinks/module","./annotations/all","./templating/all","./dashboard/all","./playlist/all","./snapshot/all","./panel/all","./styleguide/styleguide"],function(){});