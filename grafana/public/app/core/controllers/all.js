/*! grafana - v4.5.2 - 2017-09-22
 * Copyright (c) 2017 Torkel Ödegaard; Licensed Apache-2.0 */

define(["./inspect_ctrl","./json_editor_ctrl","./login_ctrl","./invited_ctrl","./signup_ctrl","./reset_password_ctrl","./error_ctrl"],function(){});