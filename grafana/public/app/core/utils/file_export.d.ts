/// <reference path="../../../../public/app/headers/common.d.ts" />
export declare function exportSeriesListToCsv(seriesList: any, dateTimeFormat?: String, excel?: boolean): void;
export declare function exportSeriesListToCsvColumns(seriesList: any, dateTimeFormat?: String, excel?: boolean): void;
export declare function exportTableDataToCsv(table: any, excel?: boolean): void;
export declare function saveSaveBlob(payload: any, fname: any): void;
