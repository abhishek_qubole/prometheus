/*! grafana - v4.5.2 - 2017-09-22
 * Copyright (c) 2017 Torkel Ödegaard; Licensed Apache-2.0 */

!function(a){"use strict";var b=a.createElement("STYLE"),c="addEventListener"in a,d=function(b,d){c?a.addEventListener(b,d):a.attachEvent("on"+b,d)},e=function(a){b.styleSheet?b.styleSheet.cssText=a:b.innerHTML=a};a.getElementsByTagName("HEAD")[0].appendChild(b),d("mousedown",function(){e(":focus{outline:0 !important}::-moz-focus-inner{border:0;}")}),d("keydown",function(){e("")})}(document);