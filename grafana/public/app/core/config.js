/*! grafana - v4.5.2 - 2017-09-22
 * Copyright (c) 2017 Torkel Ödegaard; Licensed Apache-2.0 */

define(["app/core/settings"],function(a){"use strict";var b=window.grafanaBootData||{settings:{}},c=b.settings;return c.bootData=b,new a(c)});