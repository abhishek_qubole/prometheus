/// <reference path="../../../../public/app/headers/common.d.ts" />
export declare class AlertSrv {
    private $timeout;
    private $sce;
    private $rootScope;
    private $modal;
    list: any[];
    /** @ngInject */
    constructor($timeout: any, $sce: any, $rootScope: any, $modal: any);
    init(): void;
    getIconForSeverity(severity: any): "fa fa-exclamation" | "fa fa-check" | "fa fa-exclamation-triangle";
    set(title: any, text: any, severity: any, timeout: any): {
        title: any;
        text: any;
        severity: any;
        icon: string;
    };
    clear(alert: any): void;
    clearAll(): void;
    showConfirmModal(payload: any): void;
}
