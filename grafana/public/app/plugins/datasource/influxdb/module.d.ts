import InfluxDatasource from './datasource';
import { InfluxQueryCtrl } from './query_ctrl';
declare class InfluxConfigCtrl {
    static templateUrl: string;
}
declare class InfluxAnnotationsQueryCtrl {
    static templateUrl: string;
}
export { InfluxDatasource as Datasource, InfluxQueryCtrl as QueryCtrl, InfluxConfigCtrl as ConfigCtrl, InfluxAnnotationsQueryCtrl as AnnotationsQueryCtrl };
