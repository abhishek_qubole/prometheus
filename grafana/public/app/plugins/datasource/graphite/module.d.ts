import { GraphiteDatasource } from './datasource';
import { GraphiteQueryCtrl } from './query_ctrl';
import { GraphiteConfigCtrl } from './config_ctrl';
declare class AnnotationsQueryCtrl {
    static templateUrl: string;
}
export { GraphiteDatasource as Datasource, GraphiteQueryCtrl as QueryCtrl, GraphiteConfigCtrl as ConfigCtrl, AnnotationsQueryCtrl as AnnotationsQueryCtrl };
