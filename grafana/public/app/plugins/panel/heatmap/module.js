/*! grafana - v4.5.2 - 2017-09-22
 * Copyright (c) 2017 Torkel Ödegaard; Licensed Apache-2.0 */

System.register(["./color_legend","./heatmap_ctrl"],function(a,b){"use strict";var c;b&&b.id;return{setters:[function(a){},function(a){c=a}],execute:function(){a("PanelCtrl",c.HeatmapCtrl)}}});